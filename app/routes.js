const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty('name')) {
			return res.status(400).send({
				'error':'Bad Request: missing required parameters NAME.'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error':'Bad Request: NAME parameter should be string.'
			})	
		}

		if(req.body.name === '' || req.body.name === null){
			return res.status(400).send({
				'error':'Bad Request: parameter NAME should not be empty.'
			})	
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error':'Bad Request: missing required parameters EXCHANGE.'
			})
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error':'Bad Request: parameters EXCHANGE should be an object.'
			})	
		}

		if(req.body.ex === null || typeof req.body.ex !== 'object' || Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				'error':'Bad Request: parameters EXCHANGE should not be empty.'
			})	
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error':'Bad Request: missing required parameters ALIAS.'
			})	
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error':'Bad Request: parameters ALIAS should be a string.'
			})	
		}

		if(req.body.alias === '' || req.body.alias === null || Object.keys(req.body.alias).length === 0){
			return res.status(400).send({
				'error':'Bad Request: parameter ALIAS should not be empty.'
			})	
		}

		let foundAlias = exchangeRates.find((currency) => {
			return currency.alias === req.body.alias
		})

		let foundName = exchangeRates.find((currency) => {
			return currency.name === req.body.name
		})

		if(foundAlias){
			return res.status(400).send({
				'error':'Bad Request: the given value of parameter ALIAS already exist.'
			})	
		}

		if(foundName){
			return res.status(400).send({
				'error':'Bad Request: the given value of parameter NAME already exist.'
			})	
		}

		return res.status(200).send({
			'success':'New currency saved successfully.'
		})
	})

module.exports = router;
